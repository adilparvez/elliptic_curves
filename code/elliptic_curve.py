class EllipticCurve(object):

    def __init__(self, coefficients, prime):
        if discriminant_in_Z(coefficients) % prime == 0:
            raise Exception("Zero discriminant.")
        self.coefficients = coefficients
        self.prime = prime
        self.size = 0

    def contains_point(self, point):
        [a1, a2, a3, a4, a6] = self.coefficients
        [x, y] = point
        lhs = y**2 + a1*x*y + a3*y
        rhs = x**3 + a2*x**2 + a4*x + a6
        return (rhs - lhs) % self.prime == 0

    def order_naive(self):
        if self.size:
            return self.size
        count = 1 # Point at infinity.
        for x in range(self.prime):
            for y in range(self.prime):
                if self.contains_point([x, y]):
                    count += 1
        self.size = count
        return count

    def order(self):
        if self.size:
            return self.size
        if self.prime == 2:
            return self.order_naive()
        else:
            [a1, a2, a3, a4, a6] = self.coefficients
            count = 1 # Point at infinity.
            for x in range(self.prime):
                discriminant = ((a1*x + a3)**2 + 4*(x**3 + a2*x**2 + a4*x + a6)) % self.prime
                if discriminant == 0:
                    count += 1
                elif jacobi(discriminant, self.prime) == 1:
                    count += 2
        self.size = count
        return count

    def tp(self):
        return self.prime + 1 - self.order()


def discriminant_in_Z(coefficients):
    [a1, a2, a3, a4, a6] = coefficients
    b2 = a1**2 + 4*a2
    b4 = a1*a3 + 2*a4
    b6 = a3**2 + 4*a6
    b8 = (a1**2)*a6 - a1*a3*a4 + 4*a2*a6 + a2*a3**2 - a4**2
    return -(b2**2)*b8 - 8*b4**3 - 27*b6**2 + 9*b2*b4*b6

def jacobi(a, N):
    if N <= 0 or N % 2 == 0:
        raise Exception("Illegal \"denominator\" in Jacobi symbol.")
    if a == 0:
        if N == 1:
            return 1
        else:
            return 0
    elif a == 2:
        if N % 8 == 1 or N % 8 == 7:
            return 1
        else:
            return -1
    elif a >= N:
        return jacobi(a % N, N)
    elif a % 2 == 0:
        return jacobi(2, N) * jacobi(a/2, N)
    else:
        if a % 4 == 3 and N % 4 == 3:
            return -jacobi(N, a)
        else:
            return jacobi(N, a)
