import matplotlib.pyplot as plt
from math import sqrt
import elliptic_curve as ec
import prime_utils as pu

def histogram(coefficients, p1, p2):
    discriminant = ec.discriminant_in_Z(coefficients)
    discriminant_factors = pu.factors(discriminant)
    good_primes = [p for p in pu.sieve(p2) if p >= p1 and p not in discriminant_factors]
    
    data = []
    for p in good_primes:
        curve = ec.EllipticCurve(coefficients, p)
        data.append(curve.tp() / (2 * sqrt(p)))

    plt.hist(data, bins=25)
    plt.xlabel("t_p/2sqrt(p)")
    plt.ylabel("Frequency")
    plt.show()
