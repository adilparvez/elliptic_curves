def factors(n):
    n = abs(n)
    factors = []

    while n % 2 == 0:
        factors.append(2)
        n /= 2

    divisor = 3
    while n != 1:
        while n % divisor == 0:
            factors.append(divisor)
            n /= divisor
        divisor += 2

    return factors

# List of primes less than or equal to limit, using sieve of Eratosthenes.
def sieve(limit):
    is_prime = [False] * 2 + [True] * (limit - 1)
    primes = []
    for (i, prime) in enumerate(is_prime):
        if prime:
            primes.append(i)
            for j in range(i*i, limit + 1, i):
                is_prime[j] = False
    return primes