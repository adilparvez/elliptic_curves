import elliptic_curve as ec

# Print out order of group over Fp and tp.
def info(coefficients, p):
    curve = ec.EllipticCurve(coefficients, p)
    print("order: " + str(curve.order()) + "\ntp: " + str(curve.tp()))
