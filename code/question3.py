import elliptic_curve as ec
import prime_utils as pu

# Print discriminant and its factors, then print out a table of the order and tp at good reductions.
def print_reduction_table(coefficients, p1, p2):
    discriminant = ec.discriminant_in_Z(coefficients)
    discriminant_factors = pu.factors(discriminant)
    good_primes = [p for p in pu.sieve(p2) if p >= p1 and p not in discriminant_factors]
    print("discriminant: " + str(discriminant))
    print("factors: " + str(discriminant_factors))
    border = "+" + ("-" * (8 + 1) + "+") * 3
    table = border + "\n" + "|    p    | |E(Fp)| |   tp    |\n" + border + "\n"
    for p in good_primes:
        curve = ec.EllipticCurve(coefficients, p)
        table += "|{0:>8} |{1:>8} |{2:>8} |\n".format(str(p), str(curve.order()), str(curve.tp()))
    table += border + "\n"
    print(table)

print("(a) y^2 + 4y = x^3 + x^2 + 4x")
print_reduction_table([0,1,4,4,0], 0, 200)

print("(b) y^2 + y = x^3 + 15x - 99")
print_reduction_table([0,0,1,15,-99], 0, 200)

print("(c) y^2 + y = x^3 - 66x - 212")
print_reduction_table([0,0,1,-66,-212], 0, 200)

print("(d) y^2 + y = x^3 - x^2 - 8x - 7")
print_reduction_table([0,-1,1,-8,-7], 0, 200)